# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.conf import settings
from django.core.mail import send_mail
from django.core.exceptions import ValidationError
from django.contrib import messages
import pdb

from forms import ContactForm
from models import Amazon

pro_con = [
	("good stuff", "bad stuff"),
	("good stuff", "bad stuff"),
	("good stuff", "bad stuff"),
	("good stuff", "bad stuff")
]

attributes = [
	"Cool Attribute",
	"Cool Attribute",
	"Cool Attribute",
	"Cool Attribute",
]

def home(request):
    amazon = Amazon()
    all = amazon.all()

    return render(request, "home.html", context={'results': all})

def category(request, category_name):
    amazon = Amazon()
    products = amazon.category(category_name)

    return render(request, "category.html", context={'results': products})

def product(request, asin):
    amazon = Amazon()
    item = amazon.item(asin)

    return render(request, "product.html", context={'attributes': attributes, 'pro_con': pro_con, 'results': [item]})

def contact(request):
    if request.method == 'POST':
        form = ContactForm(request.POST)

        if form.is_valid():
            name = form.cleaned_data['name']
            email = form.cleaned_data['email']
            subject = form.cleaned_data['subject']
            message = form.cleaned_data['message']
            final_message = "Name: " + name + "\n" + "Email: " + email + "\n" + "\n" + "Message: " + message
            from_email = settings.EMAIL_HOST_USER
            to_list = [settings.EMAIL_FEEDBACK_USER]

            if subject and message and from_email:
                send_mail(subject, final_message, from_email, to_list, fail_silently=False)
                messages.success(request, 'Your email has been sent!')
		context = {
		    "form": form,
		    "action": "/contact/"
		}
		return render(request, "contact_form.html", context)

        else:
            raise ValidationError(
                ('Invalid value: %(value)s'),
                code='invalid',
                params={'value': '42'},
            )

    else:
        form = ContactForm()
        context = {
            "form": form,
	    "action": "/contact/"
        }
        return render(request, "contact_form.html", context)
