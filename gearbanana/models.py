# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from helpers import Wrapper
from api.amazon import AmazonApiCached
import pdb

class AmazonProduct(models.Model):
    asin = models.CharField(max_length=9)
    name = models.CharField(max_length=100)
    date_added = models.DateField(auto_now=True)

class AmazonCategory(models.Model):
    slug = models.CharField(max_length=200)
    node_id = models.IntegerField()
    products = models.ManyToManyField(AmazonProduct)

# Dummy data, to be replaced w/ actual api call
# $(".a-link-normal").each(function(self, win) { var content = win.href; if (content.indexOf('dp') > -1) { console.log(content)} });
CATEGORIES = {
    "mouse": [
        "B01LXC1QL0",
        "B01FZ3BR5S",
        "B00E4MQODC",
        "B01798WKTY",
        "B00KKAQYXM",
        "B00HTK1NCS",
        "B01LF37K80",
        "B0086UK7IQ",
        "B01M26YUKO",
        "B01DT48W5S",
        "B075CPJPQM",
    ],
    "keyboard": [
        "B06X9J8NG4",
        "B01M4LIKLI",
        "B06XKV8R9Z",
        "B077HZ7YJ2",
        "B01N0XQI2A",
        "B016Y2BVKA",
        "B00ZUPOMDQ",
        "B01LVTI3TO",
        "B075KMZ4MX",
        "B074QP5LRW",
        "B01MRN26ES",
    ],
    "gpu": [
        "B004KABG1I",
        "B073RKHY2F",
        "B01M27X994",
        "B01M27X836",
        "B071DY2VJR",
        "B01LZYPEHK",
        "B01M5K4G60",
        "B01AZHOX5K",
        "B0049MPQA4",
        "B06XHZ29N5",
        "B004X6ABTM",
        "B01MCU1ERO",
    ],
    "controller": [
        "B077R65C4D",
        "B002B9XB0E",
        "B00OAYHIRA",
        "B06XWD8QQJ",
        "B01KJS4ILY",
        "B01GDKXMPA",
        "B016KBVBCS",
        "B01M5GG9I7",
        "B004QRKWKQ",
        "B076CG31RD",
    ],
    "headset": [
        "B012DFI02O",
        "B01L2ZRYVE",
        "B0748MTZ1C",
        "B01MDLPJ5N",
        "B076GZRS2T",
        "B00SAYCXWG",
        "B00SAYCVTQ",
        "B01E8RS598",
        "B01H2YOB6U",
        "B0748ND2HV",
    ]
}

class Amazon(object):
    def __init__(self):
        self._api = AmazonApiCached()

    def item(self, asin):
        res = self._api.ItemLookup(ItemId=asin)
        item = res.flattened
        item_stripped = Wrapper()

        key_val_list = [
                ['image', 'Items.Item.MediumImage.URL'],
                ['name', 'Items.Item.ItemAttributes.Title'],
                ['price', 'Items.Item.OfferSummary.LowestNewPrice.FormattedPrice'],
                ['amazon_url', 'Items.Item.DetailPageURL'],
                ['id', 'Items.Item.ASIN']
        ]

        for key, val in key_val_list:
            if val in item:
                item_stripped[key] = item[val]
            else:
                item_stripped[key] = "Not Available"

        item_stripped = Wrapper(
            image=item.get('Items.Item.MediumImage.URL'),
            name=item.get('Items.Item.ItemAttributes.Title'),
            #price=item.get('Items.Item.OfferSummary.LowestNewPrice.FormattedPrice'),
            price=item.get('Items.Item.ItemAttributes.ListPrice.FormattedPrice'),
            category='mouse', # needs to be dynamically assigned, currently hardcoded
            id=item.get('Items.Item.ASIN'),
        )
        return item_stripped

    def category(self, r_category):
        for category, asins in CATEGORIES.iteritems():
            if category == r_category:
                for asin in asins:
                    yield self.item(asin)

    def all(self):
        for category, asins in CATEGORIES.iteritems():
            for asin in asins:
                yield self.item(asin)
