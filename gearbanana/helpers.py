class Wrapper(object):
    def __init__(self, **kwargs):
        for kwarg, value in kwargs.iteritems():
            self.__dict__[kwarg] = value

    def __setitem__(self, key, value):
        self.__dict__[key] = value

    def __getitem(self, key, value):
        return self.__dict__[key]
