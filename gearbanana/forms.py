from django import forms

class ContactForm(forms.Form):
    name = forms.CharField(label='Your Name', max_length=50)
    email = forms.EmailField(label='Email Address')
    subject = forms.CharField(label='Subject', max_length=100)
    message = forms.CharField(widget=forms.Textarea, label='Your Message', max_length=100)
