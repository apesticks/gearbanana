from django.conf.urls import include, url
from django.contrib import admin
from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^(?P<category_name>[-\w]+)/$', views.category, name='category'),
    url(r'^product/(?P<asin>[-\w]+)/$', views.product, name='asin'),
    url(r'^contact/$', views.contact, name='contact'),

    # amazon api
    url(r'^api/', include('api.urls')),

    # admin interface
    url(r'^admin/', admin.site.urls),
    #url(r'^\.well-known/', include('letsencrypt.urls')),

    # catch-all react route
    #url(r'^.*$', react_template_view),
]
