from django.db import models

class CachedAmazonItem(models.Model):
    asin = models.CharField(max_length=250)
    last_updated = models.DateField(auto_now=True)
