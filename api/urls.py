from django.conf.urls import url
from .views import item, category, all, err

from . import views

# Only for debugging. These will not be exposed publically.
urlpatterns = [
    url(r'^id/([0-9A-Z]{10})$', item, name='id'),
    url(r'^category/([\w\-]+)$', category, name='category'),
    url(r'^all$', all, name='all'),
    url(r'^.*$', err),
]
