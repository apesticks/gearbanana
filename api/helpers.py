import collections
import pdb

SEP = "."

def groups(*items):
    return ",".join(items)

def flatten(node, parent_key='', skip=''):
    items = []
    for key, value in node.items():
        if parent_key:
            if parent_key in skip:
                new_key = key
            else:
                new_key = parent_key + SEP + key
        else:
            new_key = key

        if isinstance(value, collections.MutableMapping):
            new_node = flatten(value, new_key, skip=skip).items()
            items.extend(new_node)
        else:
            items.append((new_key, value))
    return dict(items)

class AmazonResponse(object):
    def __init__(self, response):
        self.skip_key = 'ItemLookupResponse'
        self._response = response
        self._flat = flatten(self._response, skip=self.skip_key)

    @property
    def flattened(self):
        return self._flat

    @property
    def keys(self):
        return self._flat.keys()

    def fields(self, *fields):
        result = {}
        for field in fields:
            result[field] = self.get(field, None)
        return result

    def get(self, value, default=None):
        return self._flat.get(value, default)

    def pprint(self):
        items = self.flattened.items()
        items.sort(key=lambda entry: entry[0])

        for key, value in items:
            print "{}: {}".format(key, value)

    def __repr__(self):
        return "<AmazonResponse>"
