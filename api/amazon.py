import bottlenose
import xmltodict
import json
import secrets
import pdb
import pickle
from django.conf import settings
from django.core.cache import cache
from helpers import AmazonResponse, groups

# Licensing/rate limit requirements (including but not limited to):
# 1. Ensure that images are not cached (simply link to them)
# 2. Non-images can only be cached 24 hrs
# 3. If caching is > 1 hour, messaging must include update date
#    (e.g. Price: $32.77 (as of 14:11 PST - More info))
# 4. No websites designed for mobile can use the data
# 5. Limit requests to 1 per second, unless otherwise specified
# (see Associates Program IP License - https://associates.amazon.ca/help/operating/policies)

CACHE_SETTINGS = {
    "CacheWriter": cache.set,
    "CacheReader": cache.get,
}

Parsers = {
    "dict": lambda text: xmltodict.parse(text), # Default output is an OrderedDict
    "json": lambda text: json.dumps(xmltodict.parse(text)),
    "pickle": lambda text: pickle.dumps(xmltodict.parse(text)),
}

# TODO: figure out how to optimize the requests to minimize based on needed data
RESPONSE_GROUPS = {
    'ItemLookup': groups(
        'Offers',
        'Images',
        #'EditorialReview',
        'ItemAttributes',
        #'SalesRank',
        #'BrowseNodes',
    ),
    'BrowseNodeLookup': groups(
        'TopSellers',
        #'MostWishedFor',
        #'MostGifted',
    ),
}

class AmazonApi(object):
    SECRETS = [
        secrets.AWS_ACCESS_KEY_ID,
        secrets.AWS_SECRET_ACCESS_KEY,
        secrets.AWS_ASSOCIATE_TAG,
    ]

    def __init__(self, format='dict', **kwargs):
        self._api = bottlenose.Amazon(*self.SECRETS, **kwargs)
        self.set_parser(format)

    def TopTenAsin(self, node_id, response_group='TopSellers'):
        """" For a given Amazon NodeId, return the top 10 ASINs """
        query = self._api.BrowseNodeLookup(BrowseNodeId=node_id, ResponseGroup=response_group)
        res = AmazonResponse(query)
        return res.get('BrowseNodeLookupResponse.BrowseNodes.BrowseNode.TopSellers.TopSeller')

    def BrowseNodeLookup(self, *args, **kwargs):
        """ XXX: Find a way to refactor so that the wrapped functions
        contain less boilerplate"""
        response_groups = RESPONSE_GROUPS['BrowseNodeLookup']
        query = self._api.BrowseNodeLookup(*args, ResponseGroup=response_groups, **kwargs)

        if self.format == 'dict':
            return AmazonResponse(query)

        return query

    def ItemLookup(self, *args, **kwargs):
        """ XXX: Find a way to refactor so that the wrapped functions
        contain less boilerplate"""
        response_groups = RESPONSE_GROUPS['ItemLookup']
        query = self._api.ItemLookup(*args, ResponseGroup=response_groups, **kwargs)

        if self.format == 'dict':
            return AmazonResponse(query)

        return query

    def set_parser(self, format="dict"):
        if self._api:
            try:
                self._api.Parser = Parsers[format]
            except KeyError:
                raise Exception("Invalid parser")
        self.format = format

    def get(self, field):
        return self.flattened.get(field)

class AmazonApiCached(AmazonApi):
    def __init__(self, *args, **kwargs):
        super(AmazonApiCached, self).__init__(*args, MaxQPS=0.9, **CACHE_SETTINGS)

if __name__ == "__main__":
    amazon = AmazonApi(
        format="pickle",
        MaxQPS=0.9,
        CacheWriter=cache.set,
        CacheReader=cache.get,
    )
    #amazon = AmazonApi(format="dict", MaxQPS=0.9)
    #res = amazon.ItemLookup(ItemId="B00AAS888S", ResponseGroup="Offers,Images,EditorialReview,ItemAttributes,SalesRank")
    res = amazon.ItemLookup(ItemId="B00AAS888S", ResponseGroup="Offers,Images,EditorialReview,ItemAttributes,SalesRank")
    print res
    #print res.pprint()
    #print res._response
