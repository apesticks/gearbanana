# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.http import HttpResponse
from django.core.cache import cache
from helpers import groups
import pdb
import json

from amazon import AmazonApi

# These settings do not belong here. They belong in settings.py
# But getting it to *work* is more important now than anything else

CACHE_SETTINGS = {
    "CacheWriter": cache.set,
    "CacheReader": cache.get,
}

ITEM_FIELDS = {
    'Items.Item.ASIN',
    'Items.Item.OfferSummary.LowestNewPrice.CurrencyCode',
    'Items.Item.OfferSummary.LowestNewPrice.FormattedPrice',
    'Items.Item.OfferSummary.TotalNew',
    'Items.Item.Offers.Offer.OfferAttributes.Condition',
    'Items.Item.ParentASIN',
}

NODE_MAPPING = {
    'mouse': 402052011,
    'keyboard': 402051011,
    'gpu': 284822,
    'headset': 402053011,
    'controllers': 402045011,
}

def fetch_category(category):
    # XXX: move this code somewhere else
    node_id = NODE_MAPPING[category]
    amazon = AmazonApi(MaxQPS=0.9, **CACHE_SETTINGS)

    top_ten = amazon.TopTenAsin(node_id)
    asins = [item['ASIN'] for item in top_ten]
    res = amazon.ItemLookup(ItemId=groups(*asins), **CACHE_SETTINGS)
    return res

# TODO: prevent remote-origin api requests

def item(request, asin):
    output = {}
    fields = request.GET.get('fields')

    amazon = AmazonApi(MaxQPS=0.9, **CACHE_SETTINGS)
    res = amazon.ItemLookup(ItemId=asin)

    #if fields == 'all':
    output = json.dumps(res.flattened)
    #else:
    #    output = json.dumps(res.fields(*ITEM_FIELDS))

    return HttpResponse(output, content_type="application/json")

def category(request, category):
    data = {}
    fields = request.GET.get('fields')

    if category not in NODE_MAPPING:
        output = json.dumps({'error': 'Invalid category'})
        return HttpResponse(output, content_type="application/json")
    res = fetch_category(category)
    #res = amazon.BrowseNodeLookup(BrowseNodeId=node_id)

    output = json.dumps(res.flattened)
    return HttpResponse(output, content_type="application/json")

def all(request):
    results = []
    for category in NODE_MAPPING:
        res = fetch_category(category).flattened
        results += res['Items.Item']

    output = json.dumps(results)
    return HttpResponse(output, content_type="application/json")

def err(request):
    return HttpResponse("Invalid query", status=500)
