#!/bin/sh
set -x

SITES=/home/wsgi/sites
PROJECT_NAME=gearbanana
PROJECT_PATH=$SITES/$PROJECT_NAME
HOME=$(realpath $(dirname .))

# Install nginx and uwsgi
sudo apt-get install nginx
sudo apt-get install uwsgi

# Create service-related paths
mkdir /run/uwsgi
mkdir -p /etc/uwsgi/sites

# Add isolated user for wsgi service
useradd -m -d /home/wsgi wsgi

# Create project path
mkdir $PROJECT_PATH

# Create symlink to project path
ln -s $HOME $SITES/$PROJECT_NAME

# Set future group ownership of project path to wsgi user
chmod g+s $PROJECT_PATH
setfacl -Rdm u:wsgi:rx $PROJECT_PATH
chgrp -R wsgi $PROJECT_PATH

# Propogate configuration and service symlinks
ln -s $PROJECT_PATH/conf/uwsgi.ini /etc/uwsgi/sites/gearbanana.ini
ln -s $PROJECT_PATH/conf/nginx /etc/nginx/sites-enabled/gearbanana
ln -s $PROJECT_PATH/conf/uwsgi.service /etc/systemd/system/
