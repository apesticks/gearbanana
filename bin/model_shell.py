#!/bin/bash
import os
import pdb
import sys
import django

PROJECT_PATH = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "gearbanana.settings")
sys.path.insert(0, PROJECT_PATH)
django.setup()

import gearbanana
from gearbanana.models import *

pdb.set_trace()
