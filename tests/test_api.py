import pickle
import os.path
import collections
from unittest import TestCase
from mock import Mock, call, patch
from api.amazon import AmazonApi
from api.helpers import AmazonResponse, flatten

class AmazonApiTestCase(TestCase):
    @property
    def test_method(self):
        return self._testMethodName

    @property
    def test_class(self):
        return self.__class__.__name__

    def get_fixture_name(self):
        if self.test_class == 'className':
            pass

        if self.test_method == 'foo':
            pass

        return 'offers'

    def fixture(self):
        fixture_name = self.get_fixture_name()
        filename = 'fixture_{}.pickle'.format(fixture_name)
        dirname = os.path.dirname(__file__)
        f_path = os.path.join(dirname, filename)
        with open(f_path) as f:
            return pickle.load(f)

    def setUp(self):
        try:
            self.args
        except AttributeError:
            self.args = []
        try:
            self.kwargs
        except AttributeError:
            self.kwargs = {}

        method = self.method_being_tested

        self.amazon = AmazonApi()
        self.patcher = patch('bottlenose.Amazon.{}'.format(method), return_value=self.fixture(), create=True)
        self.patcher.start()

        self.method_obj = getattr(self.amazon, self.method_being_tested)
        self.lookup = self.method_obj(*self.args, **self.kwargs)

class ItemLookupTestCase(AmazonApiTestCase):
    def setUp(self):
        self.kwargs = {
            'ItemId': 'foo',
        }
        self.method_being_tested = "ItemLookup"
        super(ItemLookupTestCase, self).setUp()

    def tearDown(self):
        self.patcher.stop()

    def test_upstream_lookup(self):
        """ Ensure that a lookup to local API creates a request to upstream API """
        self.amazon._api.ItemLookup.mock_calls[0] == call(ItemId='foo')

    def test_return_amazon_response(self):
        assert isinstance(self.lookup, AmazonResponse)

    def test_query_field(self):
        value = self.lookup.get('Items.Item.Offers.Offer.OfferListing.Price.FormattedPrice')
        value == '$44.89'

    def test_flattened(self):
        assert isinstance(self.lookup.flattened, dict)
        assert len(self.lookup.flattened.keys()) > 0

    def test_keys(self):
        assert 'ItemLookupResponse' not in self.lookup.keys[0]

    def test_multiquery(self):
        fields = [
            'Items.Item.ASIN',
            'Items.Item.OfferSummary.LowestNewPrice.CurrencyCode',
            'Items.Item.OfferSummary.LowestNewPrice.FormattedPrice',
            'Items.Item.OfferSummary.TotalNew',
            'Items.Item.Offers.Offer.OfferAttributes.Condition',
            'Items.Item.ParentASIN',
        ]
        expected = {
            'Items.Item.ASIN': u'B00AAS888S',
            'Items.Item.OfferSummary.LowestNewPrice.FormattedPrice': u'$42.92',
            'Items.Item.OfferSummary.LowestNewPrice.CurrencyCode': u'USD',
            'Items.Item.OfferSummary.TotalNew': u'22',
            'Items.Item.Offers.Offer.OfferAttributes.Condition': u'New',
            'Items.Item.ParentASIN': u'B07452BDPB',
        }
        query = self.lookup.fields(*fields)
        assert isinstance(query, dict)
        assert query == expected


class HelperTestCase(TestCase):
    def test_flatten(self):
        input = {
            "item": [1,2,3],
            "other": {
                "deep": {
                    "nest": 69,
                },
                "thing": [
                    "last",
                    "next",
                ],
                "next": 3,
            },
            "test": 1,
        }
        expected = {
            "item": [1,2,3],
            "other.thing": [
                "last",
                "next",
            ],
            "other.deep.nest": 69,
            "other.next": 3,
            "test": 1,
        }
        flatten(input) == expected
